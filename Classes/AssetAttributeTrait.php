<?php

namespace DigiComp\AssetAttributes;

use DigiComp\AssetAttributes\Domain\Model\AssetAttribute;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

trait AssetAttributeTrait
{
    /**
     * @return Collection<AssetAttribute>
     */
    public function getAttributes(): Collection
    {
        if ($this->attributes === null) {
            $this->attributes = new ArrayCollection();
        }
        return $this->attributes;
    }

    /**
     * @param Collection<AssetAttribute> $attributes
     */
    public function setAttributes(Collection $attributes): void
    {
        $this->attributes = $attributes;
    }
}
