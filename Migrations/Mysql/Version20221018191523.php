<?php

declare(strict_types=1);

namespace Neos\Flow\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Platforms\MySqlPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221018191523 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof MySqlPlatform,
            "Migration can only be executed safely on '\Doctrine\DBAL\Platforms\MySqlPlatform'."
        );

        $this->addSql('CREATE TABLE digicomp_assetattributes_domain_model_assetattribute (persistence_object_identifier VARCHAR(40) NOT NULL, name VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, urlvalue VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_2EFCAB7C5E237E061D775834 (name, value), PRIMARY KEY(persistence_object_identifier))');
        $this->addSql('CREATE TABLE neos_media_domain_model_asset_attributes_join (media_asset VARCHAR(40) NOT NULL, assetattributes_assetattribute VARCHAR(40) NOT NULL, INDEX IDX_68EDD3AD1DB69EED (media_asset), INDEX IDX_68EDD3ADE9AACB21 (assetattributes_assetattribute), PRIMARY KEY(media_asset, assetattributes_assetattribute))');
        $this->addSql('ALTER TABLE neos_media_domain_model_asset_attributes_join ADD CONSTRAINT FK_68EDD3AD1DB69EED FOREIGN KEY (media_asset) REFERENCES neos_media_domain_model_asset (persistence_object_identifier)');
        $this->addSql('ALTER TABLE neos_media_domain_model_asset_attributes_join ADD CONSTRAINT FK_68EDD3ADE9AACB21 FOREIGN KEY (assetattributes_assetattribute) REFERENCES digicomp_assetattributes_domain_model_assetattribute (persistence_object_identifier)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof MySqlPlatform,
            "Migration can only be executed safely on '\Doctrine\DBAL\Platforms\MySqlPlatform'."
        );

        $this->addSql('ALTER TABLE neos_media_domain_model_asset_attributes_join DROP FOREIGN KEY FK_68EDD3ADE9AACB21');
        $this->addSql('DROP TABLE digicomp_assetattributes_domain_model_assetattribute');
        $this->addSql('DROP TABLE neos_media_domain_model_asset_attributes_join');
    }
}
