DigiComp.AssetAttributes
------------------------

![Build status](https://ci.digital-competence.de/api/badges/Packages/DigiComp.AssetAttributes/status.svg)

This package allows you to extend Neos Media assets with custom attributes.

This extension overwrites the original edit template of `neos/media-browser` - that way you get all of your custom properties and matching form fields in the classic asset editor.

You can add new attributes, by adding them to your `Settings.yaml`:


```yaml
DigiComp:
  AssetAttributes:
    customAssetProperties:
      author:
        type: 'textarea' # or empty for textfields
        position: 'end'
```

Each asset instance will get an "attributes" property introduced, you can work with in PHP or DQL.

Examples:

- Working with Asset instances:
    ```php
    $assetObject->getAttributes()->set($key, new AssetAttribute($key, $value));
    ```

- querying with DQL:
    ```dql
    SELECT att FROM Neos\Media\Domain\Model\Asset a JOIN a.attributes att 
    ```

- working with query objects:
    ```php
    $query = new \Neos\Flow\Persistence\Doctrine\Query(\Neos\Media\Domain\Model\Asset::class);
    $query->setOrderings(['attributes.value' => \Neos\Flow\Persistence\QueryInterface::ORDER_ASCENDING]);
    ```
