<?php

namespace DigiComp\AssetAttributes\ViewHelpers;

use Neos\Flow\Annotations as Flow;
use Neos\FluidAdaptor\Core\ViewHelper\AbstractViewHelper;
use Neos\Utility\PositionalArraySorter;

class CustomAttributesViewHelper extends AbstractViewHelper
{
    /**
     * @Flow\InjectConfiguration(path="customAssetProperties")
     * @var array|null
     */
    protected ?array $customAssetProperties;

    public function render(): array
    {
        return (new PositionalArraySorter($this->customAssetProperties))->toArray();
    }
}
