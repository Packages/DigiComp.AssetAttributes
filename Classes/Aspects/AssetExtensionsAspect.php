<?php

namespace DigiComp\AssetAttributes\Aspects;

use DigiComp\AssetAttributes\Domain\Model\AssetAttribute;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Neos\Flow\Annotations as Flow;

/**
 * @Flow\Aspect
 * @Flow\Introduce("class(Neos\Media\Domain\Model\Asset)", traitName="DigiComp\AssetAttributes\AssetAttributeTrait")
 */
class AssetExtensionsAspect
{
    /**
     * @Flow\Introduce("class(Neos\Media\Domain\Model\Asset)")
     * @ORM\ManyToMany(inversedBy="assets", indexBy="name", cascade={"persist"})
     * @var Collection<AssetAttribute>
     */
    protected $attributes;
}
