<?php

declare(strict_types=1);

namespace DigiComp\AssetAttributes\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use Neos\Flow\Annotations as Flow;

/**
 * @Flow\ValueObject(embedded=false)
 * @ORM\Table(
 *     indexes={
 *         @ORM\Index(columns={"name", "value"})
 *     }
 * )
 */
class AssetAttribute
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * @var string
     */
    protected string $value;

    /**
     * @var string
     */
    protected string $urlValue;

    /**
     * @Flow\Transient
     * @Flow\InjectConfiguration(package="DigiComp.AssetAttributes", path="urlReplacements")
     * @var array
     */
    protected array $replacementMap;

    /**
     * @param string $name
     * @param string $value
     * @param string $urlValue
     */
    public function __construct(string $name, string $value, string $urlValue = '')
    {
        $this->name = $name;
        $this->value = $value;
        if (!$urlValue) {
            $urlValue = $value;
        }
        $this->urlValue = $urlValue;
    }

    public function initializeObject(): void
    {
        if ($this->urlValue === $this->value) {
            $this->urlValue = \str_replace(
                \array_column($this->replacementMap, 'key'),
                \array_column($this->replacementMap, 'value'),
                $this->urlValue
            );
            $this->urlValue = \urlencode(\strtolower($this->urlValue));
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getUrlValue(): string
    {
        return $this->urlValue;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName() . ': ' . $this->getValue();
    }
}
