<?php

declare(strict_types=1);

namespace Neos\Flow\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221211170135 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE digicomp_assetattributes_domain_model_assetattribute DROP INDEX UNIQ_2EFCAB7C5E237E061D775834, ADD INDEX IDX_2EFCAB7C5E237E061D775834 (name, value)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE digicomp_assetattributes_domain_model_assetattribute DROP INDEX IDX_2EFCAB7C5E237E061D775834, ADD UNIQUE INDEX UNIQ_2EFCAB7C5E237E061D775834 (name, value)');
    }
}
